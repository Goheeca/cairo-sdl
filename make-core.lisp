(if (> (length sb-ext:*posix-argv*) 1)
    (progn
      (ql:quickload :cl-cairo2)
      (ql:quickload :lispbuilder-sdl)

      (sb-ext:save-lisp-and-die (second sb-ext:*posix-argv*)))
    (format t "Enter the filename of newly created core."))
