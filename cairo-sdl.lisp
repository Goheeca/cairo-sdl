;#!/usr/bin/sbcl --script

(require :asdf)
(require :cl-cairo2)
(require :lispbuilder-sdl)


(defpackage :user
  (:use :cl #|:sdl|# :cl-cairo2 :cl-colors :cffi)
  (:export :main))

(in-package :user)

(defun foreign-surface (&optional (surface sdl:*default-surface*))
  (sdl::fp surface))

(defun pixels (surface)
  (foreign-slot-value (foreign-surface surface)
		      '(:struct sdl-cffi::sdl-surface)
		      'sdl-cffi::pixels))

(defun pitch (surface)
  (foreign-slot-value (foreign-surface surface)
		      '(:struct sdl-cffi::sdl-surface)
		      'sdl-cffi::pitch))

(defun pixel-format (surface)
  (foreign-slot-value (foreign-surface surface)
		      '(:struct sdl-cffi::sdl-surface)
		      'sdl-cffi::format))

#|
(defun bpp (surface)
  (foreign-slot-value (sdl-cffi::pixel-format (foreign-surface surface))
		      '(:struct sdl-cffi::sdl-pixel-format)
		      'sdl::bytesperpixel))

(defun flags (surface)
  (foreign-slot-value (foreign-surface surface)
		      '(:struct sdl-cffi::sdl-surface)
		      'sdl-cffi::flags))
|#

(defun create-sdl-surface (width height)
  (let* ((*standard-output* (make-broadcast-stream))
	 (surface (sdl:create-surface width height
				      :mask '(#x00FF0000 #x0000FF00 #x000000FF #x00000000)))
	 (sdl:*default-surface* surface))
    (values
     (create-image-surface-for-data (pixels surface)
				    :rgb24
				    (sdl-base:surf-w (foreign-surface))
				    (sdl-base:surf-h (foreign-surface))
				    (pitch surface))
     surface)))


(defconstant +width+ 800)
(defconstant +height+ 480)
(defconstant +icon+ #p"icon.bmp")
(defconstant +title+ "Cairo & SDL")
(defvar *width* +width+)
(defvar *height* +height+)
(defvar *surface*)

(defun init%% () 
  (multiple-value-bind (cairo sdl) (create-sdl-surface *width* *height*)
    (setf *surface* cairo)
    (setf sdl:*default-surface* sdl))
  
  ;;(setf *context* (create-xlib-image-context +width+ +height+))
  (setf *context* (create-context *surface*)))

(defun set-title ()
  (sdl:set-caption +title+ +title+))
  
(defun init% ()
  (sdl:window *width* *height* :fullscreen nil :resizable t)
  (set-title)
  (init%%))

(defmacro with-saved (&body body)
  `(progn (save) ,@body (restore)))

(defun render ()
  (surface-flush (get-target *context*))
  (sdl:blit-surface sdl:*default-surface* sdl:*default-display*)
  (sdl:update-display))

(defun clear (&optional (color cl-colors:+black+))
  (with-saved
    (set-source-color color)
    (paint)))

(defun make-circle ()
  (with-saved
    (arc (/ (width *context*) 2)
	 (/ (height *context*) 2)
	 (- (/ (min (width *context*) (height *context*)) 2)
	    (/ (get-line-width) 2))
	 0 (* 2 pi))
    (stroke)))

(defun make-rectangle ()
  (with-saved
    (rectangle (/ (width *context*) 4)
	       (/ (height *context*) 4)
	       (/ (width *context*) 2)
	       (/ (height *context*) 2))
    (stroke)))

(defun make-dot ()
  (with-saved
    (arc (/ (width *context*) 2)
	 (/ (height *context*) 2)
	 (/ (get-line-width) 2)
	 0 (* 2 pi))
    (stroke)))


(defun show-coords (state x y how) 
  (with-saved
    (move-to 50 100)
    (select-font-face "Arial" :normal how)
    (set-font-size 36)
    (set-source-rgba 1 0 1 0.25)
    (show-text (format nil "{~a} @ [~a,~a]" state x y))))


(defun show-char (char key) 
  (with-saved
    (move-to 50 100)
    (select-font-face "Courier New" :normal :normal)
    (set-font-size 36)
    (set-source-rgb 1 1 0)
    (show-text (format nil "~a [~a]" char key))))

(defun scene ()
  (with-saved
    (stroke) ;;; because of arc bug?
    (set-line-width 10)
    (set-source-rgb 0 1 0)
    (make-circle)
    (set-source-rgb 1 0 0)
    (make-rectangle)
    (set-source-rgb 0 0 1)
    (make-dot)
    
    (set-source-rgb 1 1 1)
    (select-font-face "Courier New" :normal :bold)
    (set-font-size 16)
    (move-to 10 10)
    (show-text "Cairo in Common Lisp via SDL.")
    
    (set-source-rgba 0 1 1 0.5)
    (select-font-face "Arial" :italic :normal)
    (set-font-size 24)
    (move-to 20 20)
    (show-text "F.O.A.D.")))

(defun draw () 
  (clear)
  (scene))

(defun free (ctx srf sdl-srf)
  (destroy ctx)
  (destroy srf)
  (sdl:free sdl-srf))

(defun resize (w h)
  (setf *width* w *height* h)
  (free *context* *surface* sdl:*default-surface*)
  (init%)
  (draw)
  (render))

;;; workaround for dynamic resizing
(defun resize-filter (event) 
  (let ((w (sdl::video-resize-w event))
	(h (sdl::video-resize-h event)))
    (resize w h))
  nil)

(defun set-icon (&aux icon colorkey)
  (setf icon (sdl:load-image +icon+))
  (setf colorkey (sdl-cffi::sdl-map-rgb (pixel-format icon) 255 0 255))
  (sdl-cffi::sdl-set-color-key (foreign-surface icon)
			       sdl-cffi::sdl-src-color-key colorkey)
  (sdl-cffi::sdl-wm-set-icon (foreign-surface icon) (null-pointer)))

(defun init ()
  (sdl:init-video)
  (sdl:enable-key-repeat nil nil)
  (sdl:enable-unicode)
  (sdl:set-event-filter :video-resize-event #'resize-filter)
  (init%)
  (set-icon))

(defun exec-quit ()
  (sdl:with-events (:wait)
      (:quit-event () t)
      (:mouse-motion-event (:state state :x x :y y)
			   (draw)
			   (show-coords state x y :normal)
			   (render))
      (:mouse-button-down-event (:button button :x x :y y)
				(format t "~&~a click @ [~a,~a]~%"
					(case button
					  (1 "left")
					  (2 "middle")
					  (3 "right")
					  (4 "up")
					  (5 "down")
					  (t "unknown"))
					x y)
				(force-output)
				(draw)
				(show-coords button x y :bold)
				(render))

      (:key-down-event (:key key :unicode unicode)
		       (format t "~a" (code-char unicode)) ; TODO: make it portable
		       (force-output)
		       (when (sdl:key= key :sdl-key-escape)  
			 (sdl:push-quit-event))
		       (draw)
		       (show-char unicode key)
		       (render)) 
      (:video-resize-event (:w w :h h) 			   
			   (resize w h))
      (:video-expose-event () (sdl:update-display)))

  (sdl:quit-sdl))

(defun main ()
  (init)
  (draw)
  (render)
  (exec-quit))

(main)
