# README #

### What is this repository for? ###

* to toy with the conjugation of Cairo and SDL

### How do I get set up? ###

* CL implementation
* to have Quicklisp
* dependencies are lispbuilder-sdl and cl-cairo2

If you have SBCL you can use the make script to create the core file (the first argument is the path of the newly created core).
The run script takes the path to core file as the first argument.