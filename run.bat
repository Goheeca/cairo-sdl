@ECHO OFF
IF [%1]==[] (
   sbcl --noinform --load cairo-sdl.lisp --quit
) ELSE (
  sbcl --core %1 --noinform --load cairo-sdl.lisp --quit
)
