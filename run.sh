#!/bin/bash

if [ -z $1 ] ; then 
    rlwrap sbcl --noinform --load cairo-sdl.lisp --quit
else 
    rlwrap sbcl --core $1 --noinform --load cairo-sdl.lisp --quit
fi
